# General options to lead for data
from Configurables import DaVinci

DaVinci().InputType = 'DST'
DaVinci().Simulation = True
DaVinci().Lumi = False
