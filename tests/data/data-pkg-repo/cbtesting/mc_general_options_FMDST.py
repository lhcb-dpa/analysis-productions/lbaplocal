# General options to lead for data
from Configurables import DaVinci

DaVinci().InputType = 'MDST'
DaVinci().Simulation = True
DaVinci().Lumi = False
